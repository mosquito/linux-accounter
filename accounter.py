#!/usr/bin/env python
#-*- coding: utf-8 -*-

from paramiko import SSHClient, AutoAddPolicy
from os import listdir

def useradd(username, shell="/bin/bash", groups="users", args="", key=""):
    c = list()
    c.append("useradd %(username)s -m -s %(shell)s -G %(groups)s %(args)s" % {'username': username, 'shell': shell, 'groups': groups, 'args': args})
    c.append("mkdir -p /home/%(username)s/.ssh" % {'username': username})
    c.append("mkdir -p /root/.ssh")
    c.append("""echo "%(key)s" >> /root/.ssh/authorized_keys """ % {'key': key})
    c.append("""echo "%(key)s" >> /home/%(username)s/.ssh/authorized_keys """ % {'key': key, 'username': username})
    c.append("chown -Rv %(username)s /home/%(username)s/.ssh """ % {'username': username})
    c.append("exit")

    return c

if __name__ == '__main__':
    from sys import argv

    if len(argv)<4:
        print "Usage:"
        print "    %s IP.ADD.RE.SS username password" % (argv[0])
        exit(1)

    conn = SSHClient()
    conn.set_missing_host_key_policy(AutoAddPolicy())
    conn.connect(argv[1], username=argv[2], password=argv[3])

    keys = {}
    for f in listdir("keys"):
        keys[f] = open("keys/%s" % f, "r").read().replace("\n", '')

    commands = list()

    for user, key in keys.items():
        for com in useradd(user, groups="adm,users,root", key=key):
            commands.append(com)

    for com in commands:
        print com
        stdin, stdout, stderr = conn.exec_command(com)
